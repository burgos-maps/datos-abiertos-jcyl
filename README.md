# datos

Datos abiertos Junta Castilla y León

## Licencia

### Datos modificados (GNU):

Los datos modificados por Burgos Maps (conversion de json a geojson etc) a partir de los datos obtenidos de la Junta de Castilla y León estan distribuidos con licencia GNU GENERAL PUBLIC LICENSE.

### Datos originales (CC-by 3.0):

La utilización, reproducción, modificación o distribución de los conjuntos de datos supone, bajo los términos de la licencia Creative Commons - Reconocimiento (CC-by 3.0), la obligación de reconocer/citar al Gobierno de Castilla y León como la fuente de los conjuntos de datos de la forma siguiente:

Fuente de los datos: Junta de Castilla y León

Si se incluye esta cita en formato HTML, puede utilizar el marcado siguiente, o similar: <p>Fuente de los datos: <a href="http://datosabiertos.jcyl.es" title="Datos Abiertos de Castilla y León">Junta de Castilla y León</a>.</p>

Asimismo se recomienda mencionar la fecha de la última actualización de los conjuntos de datos.


https://datosabiertos.jcyl.es/web/es/catalogo-datos/terminos.html

<a rel="license" href="http://creativecommons.org/licenses/by/3.0/es/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/es/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/es/">Creative Commons Attribution 3.0 Spain License</a>.
