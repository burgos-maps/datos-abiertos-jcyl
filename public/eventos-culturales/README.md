# EVENTOS DE LA AGENDA CULTURAL CATEGORIZADOS Y GEOLOCALIZADOS

Relación de eventos de la agenda cultural que se celebran en Castilla y León desde el día en el que se realiza la consulta en adelante.

Cada registro incluye su categoría, la dirección del lugar de celebración y las coordenadas de su posición geográfica.

## LICENCIAS Y ATRIBUCION

| Datos | Enlace Fuente | Licencia | Socrata |
| :------ |:--- | :--- | :--- |
| Junta de Castilla y León | https://analisis.datosabiertos.jcyl.es/Cultura-y-ocio/Eventos-de-la-agenda-cultural-categorizados-y-geol/6ei4-jd8c | https://datosabiertos.jcyl.es/web/jcyl/RISP/es/Plantilla100/1284235967637/_/_/_ |https://dev.socrata.com/foundry/analisis.datosabiertos.jcyl.es/ft3a-5csh|

socrata API endpoint:

- https://analisis.datosabiertos.jcyl.es/resource/ft3a-5csh.json

socrata API endpoint w/ filter:

- https://analisis.datosabiertos.jcyl.es/resource/ft3a-5csh.json?nombrelocalidad=Burgos
- https://analisis.datosabiertos.jcyl.es/resource/ft3a-5csh.json?c_digoprovincia=9
- https://analisis.datosabiertos.jcyl.es/resource/ft3a-5csh.json?$where=c_digoprovincia==9
