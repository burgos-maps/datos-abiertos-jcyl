# Número de nacimientos en Castilla y León

Número de nacimientos por provincias en los últimos años - Movimiento Natural de la Población

## LICENCIAS Y ATRIBUCION

| Datos | Enlace Fuente | Licencia | Socrata |
| :------ |:--- | :--- | :--- |
| Junta de Castilla y León | https://dev.socrata.com/foundry/analisis.datosabiertos.jcyl.es/8xev-vjik | https://datosabiertos.jcyl.es/web/jcyl/RISP/es/Plantilla100/1284235967637/_/_/_ |https://dev.socrata.com/foundry/analisis.datosabiertos.jcyl.es/8xev-vjik|

## socrata API endpoint:

- https://analisis.datosabiertos.jcyl.es/resource/8xev-vjik.json

socrata API endpoint w/ filter:

- https://analisis.datosabiertos.jcyl.es/resource/8xev-vjik.json?provincia=BURGOS
- https://analisis.datosabiertos.jcyl.es/resource/8xev-vjik.json?a_o=1980-01-01T00:00:00.000

## Burgos Maps:

- [8xev-vjik.json](https://burgos-maps.gitlab.io/datos-abiertos-jcyl/numero-nacimientos/8xev-vjik.json)
- [8xev-vjik.metadata.json](https://burgos-maps.gitlab.io/datos-abiertos-jcyl/numero-nacimientos/8xev-vjik.metadata.json)
